import { findAuthorsByBookIdsLoader } from './author';
import { findAuthorsByBookIdsLoader } from './book';
import { findUserByIdsLoader } from './user';
import { findReviewsByBookIdsLoader } from './review';

export default () => ({
    findAuthorsByBookIdsLoader: findAuthorsByBookIdsLoader(),
    findAuthorsByBookIdsLoader: findAuthorsByBookIdsLoader(),
    findUserByIdsLoader: findUserByIdsLoader(),
    findReviewsByBookIdsLoader: findReviewsByBookIdsLoader(),
});