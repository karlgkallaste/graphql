import express from 'express';
import { graphqlExpress, graphiqlExpress } from 'apollo-server-express';
import cors from 'cors';
import bodyParser from 'body-parser';
import { makeExecutableSchema } from 'graphql-tools';
import { graphql } from 'graphql';
import resolvers from './resolvers';
import typeDefs from './typeDefs';
import loaders from './loader';



const schema = makeExecutableSchema({ typeDefs, resolvers });

const app = express();

app.use('/graphql', 
  bodyParser.json(), 
  graphqlExpress(() => ({ 
    schema,
    context: {
      loaders: loaders()
    }
  }))
);

app.use('/graphql', bodyParser.json(), graphqlExpress({ schema }));

app.use('/graphiql', graphiqlExpress({ endpointURL: '/graphql'}));

app.listen(4000, () => {
  console.log('goto localhost:4000/graphiql');
});
